/**
 * Represents the LED display in its entity.
 * The display consists of five leds: white, blue, green, yellow, red.
 */
#include <Arduino.h>
#include "CCS811Operation.h"
#include "LedDisplay.h"
#include "hardware.h"


/**
 * Set port to output, init state of port.
 */
LedDisplay::LedDisplay()
{
  CurrentLed = SignalLed(LED_WHITE);

  LedWhite = SignalLed(LED_WHITE);
  LedBlue = SignalLed(LED_BLUE);
  LedGreen = SignalLed(LED_GREEN);
  LedYellow = SignalLed(LED_YELLOW);
  LedRed = SignalLed(LED_RED);

  LedWhite.setLowerBrightness(30);
  LedWhite.setDimSpeed(2);
  LedBlue.setDimSpeed(4);
  LedGreen.setDimSpeed(4);
  LedYellow.setDimSpeed(4);
  LedRed.setDimSpeed(4);
}

/**
 * @brief Sets range of the leds.
 */
void LedDisplay::setup(int topvalue)
{
  int nextHi = topvalue;
  Serial.println("Thresholds ");
  Serial.println(nextHi);
  nextHi = this->calculateAndSetThresholds(nextHi, LedRed);
  Serial.println(nextHi);
  nextHi = this->calculateAndSetThresholds(nextHi, LedYellow);
  Serial.println(nextHi);
  nextHi = this->calculateAndSetThresholds(nextHi, LedGreen);
  Serial.println(nextHi);
  nextHi = this->calculateAndSetThresholds(nextHi, LedBlue);
}

/**
 * @brief Calculates the lower threshold and hysteresis from the given upper level.
 * 
 * @param hiLevel 
 * @param Led 
 * @return int 
 */
int LedDisplay::calculateAndSetThresholds(int hiLevel, SignalLed &Led)
{
  int lowLevel = hiLevel * 40 / 100;
  int hysteresis = (int) (lowLevel/10);
  Led.Range.setRange(lowLevel, hiLevel);
  return (lowLevel + hysteresis);
}


void LedDisplay::tick1ms()
{
  LedWhite.tick1ms();
  LedBlue.tick1ms();
  LedGreen.tick1ms();
  LedYellow.tick1ms();
  LedRed.tick1ms();
}


void LedDisplay::setCurrentLed(SignalLed Current)
{
    this->CurrentLed = Current;
}

/**
 * Displays the air quality indicator according to status and measurements.
 * 
 * blue = good 
 * green = acceptable
 * yellow = bad
 * red = critical
 */
void LedDisplay::displayAirQuality(byte sensorState, int co2, int voc)
{
    // white = status led
    if (sensorState == STATE_SENSOR_FAIL ) {
      //sensorErrorCode = error;
      LedWhite.blink(3);
    }
    // readings within the first 20min are not reliable
    else if (sensorState == STATE_WARM_UP ) {
      LedWhite.blink(1);
    }
    else {
      LedWhite.on();
    }

    // Red
    if (LedRed.Range.overflows((int)voc)) {
      setOneOfAll(LedRed);
    }
    else if (LedRed.Range.isInRange((int)voc)) {
      LedRed.dimOn();
    }
    else {
      LedRed.dimOff();
    }

    // Yellow
    if (LedYellow.Range.isInRange((int)voc)) {
      LedYellow.dimOn();
    }
    else {
      LedYellow.dimOff();
    }

    // Green
    if (LedGreen.Range.isInRange((int)voc)) {
      LedGreen.dimOn();
    }
    else {
      LedGreen.dimOff();
    }

    // Blue
    if (LedBlue.Range.isInRange((int)voc)) {
      LedBlue.dimOn();
    }
    else if (LedBlue.Range.underflows((int)voc)) {
      // lower than expected
      setOneOfAll(LedBlue);
    }
    else {
      LedBlue.dimOff();
    }
}


/**
 * Sets the given Led to shine.
 * Darkens all other.
 */
void LedDisplay::setOneOfAll(SignalLed led)
{
  int portToSet = led.port;

  if (LedBlue.equalsPort(portToSet)) {
    LedBlue.dimOn();
    LedGreen.dimOff();
    LedYellow.dimOff();
    LedRed.dimOff();
  }
  else if (LedGreen.equalsPort(portToSet)) {
    LedBlue.dimOff();
    LedGreen.dimOn();
    LedYellow.dimOff();
    LedRed.dimOff();
  }
  else if (LedYellow.equalsPort(portToSet)) {
    LedBlue.dimOff();
    LedGreen.dimOff();
    LedYellow.dimOn();
    LedRed.dimOff();
  }
  else if (LedRed.equalsPort(portToSet)) {
    LedBlue.dimOff();
    LedGreen.dimOff();
    LedYellow.dimOff();
    LedRed.dimOn();
  }
  else {
    // error: unknown Led
    Serial.print("Failed to set unknown led: ");
    Serial.println(portToSet);
  }
}
