

#include "Segment.h"

#ifndef __SIGNALLED_H__
#define __SIGNALLED_H__


class SignalLed {
private:
  unsigned int counter;
  /** these are analog values for on- and off-state */
  int lowerBrightness = 0, upperBrightness = 255;
  int currentBrightness, targetBrightness;
  /** A high dim-speed value produces a blinking, non-dimming behaviour. Default is blink mode */
  int dimSpeed = 255;
  int blinkCodeCount;
  void slowTick();
  void initPort(int port);

public:
  SignalLed();
  SignalLed(int port);
  SignalLed(int port, int dimSpeed);
  int port;
  Segment Range;
  void setLowerBrightness(int lower);
  void setDimSpeed(int dimSpeed);
  bool equalsPort(int port);
  void on();
  void off();
  void dimOn();
  void dimOff();
  void blink(int blinkCount);
  void tick1ms();
};

#endif
