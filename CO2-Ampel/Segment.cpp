/**
 * A Segment corresponds to a mathematical interval.
 * The upper an lower border can be set and
 * it can be tested if a value is inside or outside the segment.
 */
#include "Segment.h"


Segment::Segment()
{
  this->setRange(0,0);
}

Segment::Segment(int lower, int upper)
{
  this->setRange(lower, upper);
}


void Segment::setRange(int lower, int upper)
{
  this->lowerThreshold = lower;
  this->upperThreshold = upper;
}

/**
 * Interval includes borders
 */
bool Segment::isInRange(int value) 
{
  if (value > this->upperThreshold) {
      return false;
  }
  if (value < this->lowerThreshold) {
      return false;
  }
  return true;
}

/**
 * Interval includes borders
 */
bool Segment::overflows(int value) 
{
  if (value > this->upperThreshold) {
      return true;
  }
  else {
    return false;
  }
}

/**
 * Interval includes borders
 */
bool Segment::underflows(int value) 
{
  if (value < this->lowerThreshold) {
      return true;
  }
  else {
    return false;
  }
}
