
#ifndef __HARDWARE_H__
#define __HARDWARE_H__

// white = status
// blue = good 
// green = acceptable
// yellow = bad
// red = critical
#define LED_WHITE 3
#define LED_BLUE 5
#define LED_GREEN 6
#define LED_YELLOW 9
#define LED_RED 10

// CCS811 Address. 
// 0x5A Alternate I2C Address
// 0x5B Default I2C Address
#define CCS811_ADDR 0x5A

#endif
