/**
 * System LED management.
 * The sytem Led is able to show a blink code.
 */
#include <Arduino.h>
#include "SystemLed.h"

#define LED_ON HIGH
#define LED_OFF LOW


/**
 * Set port to output, init state of port.
 */
SystemLed::SystemLed(int port) 
{
  this->port = port;
  // LED setup
  pinMode(port, OUTPUT);
  digitalWrite(port, LOW);
}


void SystemLed::setBlinkCode(int blinkCode)
{
  this->blinkCodeCount = blinkCode;
}

/**
 * Reduces load by reducing call frequency.
 * However, the resolution of 1ms is still available in this class.
 */
void SystemLed::routine1ms()
{
  this->counter++;
  if (this->counter % 64 == 0) {
    this->routineLowFrequency();
  }
}


void SystemLed::routineLowFrequency()
{
  static unsigned int ledOnTime;
  static unsigned int ledOffTime;
  bool ledState = false;

  if (ledOnTime) {
     ledOnTime --;
     ledState = true;
  }
  else if (ledOffTime) {
     ledOffTime --;
     ledState = false;
  }
  else if (this->blinkCodeCount) {
    // process next state
     ledOnTime = 4;
     ledOffTime = 6;
     this->blinkCodeCount --;
  }
  else {
    ledState = false;
  }
  

  if (ledState) {
     digitalWrite(this->port, LED_ON);
  }
  else {
     digitalWrite(this->port, LED_OFF);
  }
}
