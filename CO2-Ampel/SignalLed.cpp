/**
 * Signal LED management.
 * This class represents a signaling LED.
 * A signal LED may display analog values by its flash frequency or grade of brightness.
 * Requires analogWrite()
 */
#include <Arduino.h>
#include "SignalLed.h"

#define LED_ON HIGH
#define LED_OFF LOW


SignalLed::SignalLed()
{
}

/**
 * Set port to output, init state of port.
 */
SignalLed::SignalLed(int port)
{
  this->port = port;
  this->initPort(port);
}

/**
 * Set port to output, init state of port, set dim speed.
 */
SignalLed::SignalLed(int port, int dimSpeed=255) 
{
  this->port = port;
  this->dimSpeed = dimSpeed;
  this->initPort(port);
}


void SignalLed::initPort(int port) 
{
  pinMode(port, OUTPUT);
  digitalWrite(port, LED_OFF);  

  Range = Segment();
}


bool SignalLed::equalsPort(int port)
{
  if (port == this->port) {
    return true;
  }
  else {
    return false;
  }
}


void SignalLed::setLowerBrightness(int lower)
{
    this->lowerBrightness = lower;
}

void SignalLed::setDimSpeed(int dimSpeed)
{
    this->dimSpeed = dimSpeed;
}


void SignalLed::on()
{
  digitalWrite(this->port, LED_ON);
  this->targetBrightness = 255;
  this->currentBrightness = 255;
}

void SignalLed::off()
{
  digitalWrite(this->port, LED_OFF);
  this->targetBrightness = 0;
  this->currentBrightness = 0;
}


void SignalLed::dimOn()
{
  this->targetBrightness = this->upperBrightness;
}

void SignalLed::dimOff()
{
  this->targetBrightness = this->lowerBrightness;
}


void SignalLed::blink(int count)
{
  this->blinkCodeCount = count;  
}


void SignalLed::tick1ms()
{
  this->counter++;
  if (this->counter % 8 == 0) {
    this->slowTick();
  }
}

/**
 * Calculates and sets current brightness value.
 */
void SignalLed::slowTick()
{
  int dimSpeed;

  dimSpeed = this->dimSpeed;
  
  if(this->currentBrightness < this->targetBrightness) {
    if(this->currentBrightness + dimSpeed > this->targetBrightness) {
      // step size exceeds target level
      this->currentBrightness = this->targetBrightness;
    }
    else {
      this->currentBrightness += dimSpeed;
    }
    analogWrite(this->port, this->currentBrightness);
  }
  else if(this->currentBrightness > this->targetBrightness) {
    if(this->currentBrightness - dimSpeed < this->targetBrightness) {
      this->currentBrightness = this->targetBrightness;
    }
    else {
      this->currentBrightness -= dimSpeed;
    }
    analogWrite(this->port, this->currentBrightness);
  }

  // evaluate a blink code
  if (this->currentBrightness == this->upperBrightness && this->blinkCodeCount) {
    // brightness is on upper border, trigger dim of led
    this->targetBrightness = this->lowerBrightness;
  }
  else if (this->currentBrightness == this->lowerBrightness && this->blinkCodeCount) {
    // brightness is on lower border, trigger shine up of led
    this->targetBrightness = this->upperBrightness;
    this->blinkCodeCount --;
  }

}
