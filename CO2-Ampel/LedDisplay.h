

#ifndef __LEDDISPLAY_H__
#define __LEDDISPLAY_H__

#include "SignalLed.h"
#include "hardware.h"

class LedDisplay {
private:
    SignalLed CurrentLed;
    SignalLed LedWhite;
    SignalLed LedBlue;
    SignalLed LedGreen;
    SignalLed LedYellow;
    SignalLed LedRed;
    void setOneOfAll(SignalLed led);
    int calculateAndSetThresholds(int hiLevel, SignalLed &Led);

public:
    LedDisplay();
    void setup(int topvalue);
    void tick1ms();
    void setCurrentLed(SignalLed Current);
    void displayAirQuality(byte sensorState, int co2, int voc);

};

#endif
