
#ifndef __CCS811OPERATION_H__
#define __CCS811OPERATION_H__

// Sensor States
#define STATE_INIT 0
#define STATE_COLD 1
#define STATE_WARM_UP 2
#define STATE_SENSOR_BUSY 4
#define STATE_RUNNING 8
#define STATE_SENSOR_FAIL 16


#endif
