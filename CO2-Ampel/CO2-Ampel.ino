/******************************************************************************
 Air quality indicator with CCS811 Sensor.

 Based on example code of Sparkfun CCS811 library.
******************************************************************************/

#include "hardware.h"
#include <Ticker.h>
#include <Wire.h>
#include <SparkFunCCS811.h>
#include "SystemLed.h"
#include "LedDisplay.h"
#include "CCS811Operation.h"

void measureAir();
void loop1ms();

CCS811 AirSensor(CCS811_ADDR);
Ticker measurementTicker(measureAir, 2000, MILLIS); 
Ticker systemTicker(loop1ms, 1, MILLIS); 
SystemLed OnBoardLed(LED_BUILTIN);
LedDisplay Display;

// Air sensor state: contains any error codes
byte sensorState = 0;
byte sensorErrorCode = 0;
unsigned int co2 = 0, voc = 0;
#define SENSOR_COLD_SECONDS 60
#define SENSOR_WARMUP_SECONDS 1200


void setup()
{
  Serial.begin(115200);
  Serial.println("CO2 Ampel");
  
  //Initalize I2C Hardware
  Wire.begin();
  
  // Ticker setup
  measurementTicker.start();
  systemTicker.start();

  // Initalize display
  Display.setup(150);
  
  //Initalize air sensor
  if (AirSensor.begin() == false)
  {
    Serial.print("CCS811 error. Please check wiring. Freezing...");
    OnBoardLed.setBlinkCode(5);
    sensorState = STATE_SENSOR_FAIL;
    sensorErrorCode = 0xFE;
    while (1);
  }
  else {
    OnBoardLed.setBlinkCode(3);    
  }
}


void loop()
{
  measurementTicker.update();
  systemTicker.update();

  // if (co2 != 0 && voc != 0) {
    // takes global measurement values and pass them to the display function
    Display.displayAirQuality(sensorState, co2, voc);
  //   co2 = 0;
  //   voc = 0;
  // }
}

/**
 * All that needs a 1ms tick.
 */
void loop1ms()
{
  systemTicker.resume();
  OnBoardLed.routine1ms();
  Display.tick1ms();
}


void measureAir() 
{
  unsigned long uptime;
  // unsigned int uptimeHours = 0,  uptimeMinutes = 0,  uptimeSeconds = 0;

  measurementTicker.resume();
  sensorState = 0;
  sensorErrorCode = 0;
  
  uptime = millis() / 1000;

  sensorErrorCode = 0;
  if (uptime < SENSOR_COLD_SECONDS)
  {
    // wait for sensor warm up
    sensorState = STATE_WARM_UP;
    int remainingSeconds = SENSOR_COLD_SECONDS - uptime;
    Serial.print("Sensor defrost ");
    Serial.println(remainingSeconds);
  }
  else if (AirSensor.checkForStatusError())
  {
    byte error = AirSensor.getErrorRegister();
    printSensorError(error);
    sensorErrorCode = error;
    sensorState = STATE_SENSOR_FAIL;
  }
  //Check to see if data is ready with .dataAvailable()
  else if (AirSensor.dataAvailable())
  {
    //If so, have the sensor read and calculate the results.
    AirSensor.readAlgorithmResults();
    co2 = (unsigned int)AirSensor.getCO2();
    voc = (unsigned int)AirSensor.getTVOC();

    printSensorMeasurement(uptime, co2, voc);
  }
  else {
    sensorState = STATE_SENSOR_BUSY;
    Serial.println("Sensor not available");
  }
}

/**
 * Prints sensor measuremnt data and the uptime.
 */
void printSensorMeasurement(uint32_t uptime, uint16_t co2, uint16_t voc)
{
  unsigned int uptimeHours = 0,  uptimeMinutes = 0,  uptimeSeconds = 0;

  // calculate fractions of time
  uptimeHours = uptime / 3600;
  // uptimeSeconds is used temporary as temporary variable
  uptimeSeconds = uptime % 3600;
  uptimeMinutes = uptimeSeconds / 60;
  uptimeSeconds = uptime % 60;

  Serial.print("CO2[");
  //Returns calculated CO2 reading
  Serial.print(co2);
  Serial.print("] tVOC[");
  //Returns calculated TVOC reading
  Serial.print(voc);
  Serial.print("] uptime[");
  //Display the time since program start
  char buffer[50];
  sprintf(buffer, "%02d:%02d:%02d]", uptimeHours, uptimeMinutes, uptimeSeconds);
  Serial.print(buffer);
  Serial.print("]");
  Serial.println();  
}

/**
 * printSensorError gets, clears, then prints the errors
 * saved within the error register.
 */
void printSensorError(byte error)
{
  if (error == 0xFF) //comm error
  {
    Serial.println("Failed to get ERROR_ID register.");
  }
  else
  {
    Serial.print("Error: ");
    if (error & 1 << 5)
      Serial.print("HeaterSupply ");
    if (error & 1 << 4)
      Serial.print("HeaterFault ");
    if (error & 1 << 3)
      Serial.print("MaxResistance ");
    if (error & 1 << 2)
      Serial.print("MeasModeInvalid ");
    if (error & 1 << 1)
      Serial.print("ReadRegInvalid ");
    if (error & 1 << 0)
      Serial.print("MsgInvalid ");
    Serial.println();
  }
}

  
