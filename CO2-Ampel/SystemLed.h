
#ifndef __SystemLed_H__
#define __SystemLed_H__

class SystemLed {
private:
  int port;
	unsigned int counter;
  int blinkCodeCount;
  void routineLowFrequency();
  
public:
  SystemLed(int port);
  void setup();
  void setBlinkCode(int blinkCode);
  void routine1ms();
};

#endif
