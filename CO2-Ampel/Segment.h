
#ifndef __Segment_H__
#define __Segment_H__


class Segment {
private:
  int lowerThreshold, upperThreshold;

public:
  Segment();
  Segment(int lower, int upper);
  bool isInRange(int value);
  bool overflows(int value);
  bool underflows(int value);
  void setRange(int lower, int upper);
};

#endif
