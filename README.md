# CO2-Ampel

This project is an air qualitiy indicator on base of Arduino nano and a CCS811 eCO2/VOC Sensor.

Its purpose is to remind of good room ventilation. This may help to reduce infective particulate material.

The measurements of the CCS811 Sensor are displayed by a simple LED display like a bargraph.

In addition, an operation indicator signals the warmup phase and error conditions.
